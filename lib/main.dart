import 'dart:io';

import 'package:file_uploader/file_uplaod_api_call.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'file_uploader.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key}) : super(key: key);

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  List<File>? imageFile;

  selectImageSource() async {
    imageFile = await getImage(context);
    print("-------------- \n ${imageFile.toString()} \n -------------------");
    setState(() {});

    List<dynamic> upload = await uploadMultipleFilesUsingAPI(
        context,
        imageFile!,
        {},
        {},
        "http://192.168.0.166:3000",
        "/add/image/files",
        "files"
    );
    print("upload === ${upload}");

    for (var file in imageFile!) {
      List<dynamic> upload = await uploadFileUsingAPI(
          context,
          file,
          {},
          {},
          "http://192.168.0.166:3000",
          "/add/image/file",
          "files"
      );
      print("upload === ${upload}");
    }


    // print("Image File === $upload");
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Select File"),
      ),
      body: Center(
        child: Column(

          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            const Text(
              'Press on camera button to select source',
            ),
            if(imageFile != null)
              Container(
                margin: const EdgeInsets.only(top:20),
                child: ClipRRect(
                  child: kIsWeb
                      ? Image.network(
                        imageFile![0].path,
                        height: MediaQuery.of(context).size.height*0.35,
                        width: MediaQuery.of(context).size.height*0.35,
                        fit: BoxFit.cover,
                      )
                      : Image.file(
                          imageFile![0],
                          height: MediaQuery.of(context).size.height*0.35,
                          width: MediaQuery.of(context).size.height*0.35,
                          fit: BoxFit.cover,
                      ),
                ),
              )
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: ()=>selectImageSource(),
        tooltip: 'Camera',
        child: const Icon(Icons.camera),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
