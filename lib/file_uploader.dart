import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';

import 'package:flutter/foundation.dart';
import 'package:image_picker/image_picker.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:file_picker/file_picker.dart';

final picker = ImagePicker();

///------------- Use syntax to get Image as PickedFile -------------------------
/// @ var image = await getImage(context);
/// @ add Additional parameter is required.
/// @ Return type is "File".
/// @ Use Image.file(File file) widget to display file asset for App.
///-----------------------------------------------------------------------------

///---------------------- Packages Dependency ----------------------------------
/// @ permission_handler: ^8.2.5
/// @ image_picker: ^0.8.4+4
/// @ file_picker: ^4.2.0
///-----------------------------------------------------------------------------


///----------------------- Call Function To Start File Upload Process ----------
/// @ Process Of Implementation It will help to get Image as PickedFile
/// @ [Optional] Use maxWidth to set picked Image width text for camera source popup.
/// @ [Optional] Use maxHeight of set picked Image height text for camera source popup.
/// @ [Optional] Use headerText to set header text for camera source popup.
/// @ [Optional] Use fromGalleryText to set gallery option text for source popup.
/// @ [Optional] Use fromCameraText to set camera option text for source popup.
/// @ [Optional] Use cancelBtnText for cancel option text for source popup.
/// @ [Optional] Use enableHeader for set header color for source popup.
Future<List<File>?> getImage(BuildContext context,
    {
      String? headerText,
      Color? headerColor,
      bool? enableHeader,
      String? fromGalleryText,
      String? fromCameraText,
      String? cancelBtnText,
      double? maxWidth,
      double? maxHeight
    }
  ) async {

  List<File>? selectedImage;

  /// @ Checking condition for web platform.
  /// @ Else it will execute as mobile platform.
  if (kIsWeb) {
    int? sourceType = await selectImageSource(context, headerText:headerText, headerColor:headerColor, enableHeader:enableHeader, fromGalleryText:fromGalleryText, fromCameraText:fromCameraText, cancelBtnText:cancelBtnText);
    if(sourceType == 0 || sourceType == 1) {
      // ImageSource? imageSource = await selectImageSource(context, headerText:headerText, headerColor:headerColor, enableHeader:enableHeader, fromGalleryText:fromGalleryText, fromCameraText:fromCameraText, cancelBtnText:cancelBtnText);
      ImageSource? imageSource = sourceType==0 ? ImageSource.camera : ImageSource.gallery;
      if(imageSource != null) {
        selectedImage = await onSourceSelect(
            context,
            imageSource,
            maxWidth:maxWidth,
            maxHeight:maxHeight);
      }
    } else if(sourceType == 2) {
      selectedImage = await singleFilePicker();
    } else if(sourceType == 3) {
      selectedImage = await multipleFilePicker();
    }
  } else {
    bool storagePermissionStatus = await checkStoragePermission(context);
    if (storagePermissionStatus) {

      int? sourceType = await selectImageSource(context, headerText:headerText, headerColor:headerColor, enableHeader:enableHeader, fromGalleryText:fromGalleryText, fromCameraText:fromCameraText, cancelBtnText:cancelBtnText);
      if(sourceType == 0 || sourceType == 1) {
        // ImageSource? imageSource = await selectImageSource(context, headerText:headerText, headerColor:headerColor, enableHeader:enableHeader, fromGalleryText:fromGalleryText, fromCameraText:fromCameraText, cancelBtnText:cancelBtnText);
        ImageSource? imageSource = sourceType==0 ? ImageSource.camera : ImageSource.gallery;
        if(imageSource != null) {
          selectedImage = await onSourceSelect(
              context,
              imageSource,
              maxWidth:maxWidth,
              maxHeight:maxHeight);
        }
      } else if(sourceType == 2) {
        selectedImage = await singleFilePicker();
      } else if(sourceType == 3) {
        selectedImage = await multipleFilePicker();
      }

    } else {
      bool requestStorageStatus = await requestStoragePermission(context);
      if (requestStorageStatus) {

        int? sourceType = await selectImageSource(context, headerText:headerText, headerColor:headerColor, enableHeader:enableHeader, fromGalleryText:fromGalleryText, fromCameraText:fromCameraText, cancelBtnText:cancelBtnText);
        print("sourceType ==== ${sourceType.toString()}");
        if(sourceType == 0 || sourceType == 1) {
          // ImageSource? imageSource = await selectImageSource(context, headerText:headerText, headerColor:headerColor, enableHeader:enableHeader, fromGalleryText:fromGalleryText, fromCameraText:fromCameraText, cancelBtnText:cancelBtnText);
          ImageSource? imageSource = sourceType==0 ? ImageSource.camera : ImageSource.gallery;
          if(imageSource != null) {
            selectedImage = await onSourceSelect(
                context,
                imageSource,
                maxWidth:maxWidth,
                maxHeight:maxHeight);
          }
        } else if(sourceType == 2) {
          selectedImage = await singleFilePicker();
        } else if(sourceType == 3) {
          selectedImage = await multipleFilePicker();
        }

      }else {
        selectedImage = null;
      }
    }
  }

  return selectedImage;
}

///-------------- It will help to select Image based on source -----------------
/// @ [Optional] set maxWidth of image size
/// @ [Optional] set maxHeight of image size
Future<List<File>?> onSourceSelect(context, ImageSource source, {double? maxWidth, double? maxHeight}) async {
  List<File>? selectedFile = [];
  XFile? pickedFile = await picker.pickImage(source: source, maxWidth: maxWidth??500.0, maxHeight: maxHeight??500.0);

  // return pickedFile == null ? null : File(pickedFile.path);

  if(pickedFile == null){
    return null;
  } else {
    if(pickedFile.path ==null){
      return null;
    }else{
      selectedFile.add(File(pickedFile.path));
      return selectedFile;
    }
  }
}

///-------------------- To Select Upload Source --------------------------------
Future<int?> selectImageSource(BuildContext context,
    {
      String? headerText,
      Color? headerColor,
      bool? enableHeader,
      String? fromGalleryText,
      String? fromCameraText,
      String? cancelBtnText
    }
  ) async {
  return showModalBottomSheet(
      context: context,
      builder: (BuildContext bc){
        return Wrap(
          children: <Widget>[
            if(enableHeader==null || enableHeader)
              Container(
                  width: double.maxFinite,
                  color: Colors.blue,
                  padding: const EdgeInsets.symmetric(vertical: 20, horizontal: 10),
                  child: Text(
                      headerText??"Choose source",
                      style: const TextStyle(
                          fontSize: 16,
                          color: Colors.white
                      ),
                  )
              ),

            ListTile(
              contentPadding: const EdgeInsets.only(left: 15, right: 15),
              hoverColor: Colors.blue.withOpacity(0.1),
              leading: const Icon(Icons.camera_alt),
              title:  Text(fromCameraText??'Camera'),
              onTap: () async {
                // File selectedFile = await onSourceSelect(context, ImageSource.camera);
                Navigator.of(context).pop(0);
              },
            ),
            const Divider(
              height: 1.0,
              color: Colors.grey,
            ),

            ListTile(
                contentPadding: const EdgeInsets.only(left: 15, right: 15),
                hoverColor: Colors.blue.withOpacity(0.1),
                leading: const Icon(Icons.photo_library),
                title: Text(fromGalleryText??'Gallery'),
                onTap: () async {
                  // File selectedFile = await onSourceSelect(context, ImageSource.gallery);
                  Navigator.of(context).pop(1);
                }
            ),
            const Divider(
              height: 1.0,
              color: Colors.grey,
            ),

            ListTile(
              contentPadding: const EdgeInsets.only(left: 15, right: 15),
              hoverColor: Colors.blue.withOpacity(0.1),
              leading: const Icon(Icons.folder),
              title:  Text(fromCameraText??'Select From Drive'),
              onTap: () async {
                // File selectedFile = await onSourceSelect(context, ImageSource.camera);
                Navigator.of(context).pop(2);
              },
            ),
            const Divider(
              height: 1.0,
              color: Colors.grey,
            ),

            ListTile(
              contentPadding: const EdgeInsets.only(left: 15, right: 15),
              hoverColor: Colors.blue.withOpacity(0.1),
              leading: const Icon(Icons.folder),
              title:  Text(fromCameraText??'Upload Multiple Files From Drive'),
              onTap: () async {
                // File selectedFile = await onSourceSelect(context, ImageSource.camera);
                Navigator.of(context).pop(3);
              },
            ),
            const Divider(
              height: 1.0,
              color: Colors.grey,
            ),

            ListTile(
              contentPadding: const EdgeInsets.only(left: 15, right: 15),
              hoverColor: Colors.blue.withOpacity(0.1),
              leading: const Icon(Icons.cancel_rounded),
              title: Text(cancelBtnText??'Cancel'),
              onTap: () => Navigator.of(context).pop(null),
            ),
          ],
        );
      }
  );
}

///---------------------- Single File Picker -----------------------------------
Future<List<File>?> singleFilePicker({FileType? fileType, List<String>? allowedExtensions}) async {
  List<File>? selectedFile = [];

  FilePickerResult? result = await FilePicker.platform.pickFiles(
    type: fileType??FileType.any,
    allowedExtensions: fileType==FileType.custom?allowedExtensions??['jpg', 'pdf', 'doc']:null,
  );
  String pathString = result!.files.single.path as String;

  // return result==null?null:File(pathString);

  if(pathString == null){
    return null;
  } else {
    selectedFile.add(File(pathString));
    return selectedFile;
  }
}

///---------------------- Multiple Files Picker --------------------------------
Future<List<File>?> multipleFilePicker({
  FileType? fileType,
  List<String>? allowedExtensions
}) async {
  FilePickerResult? result = await FilePicker.platform.pickFiles(allowMultiple: true);

  List<File>? files = result?.paths.map((path) => File(path!)).toList();

  return files;
}

///-------------------- Request for Storage Permission -------------------------
Future<bool> requestStoragePermission(context) async {
  PermissionStatus requestStatus = await Permission.storage.request();
  if(requestStatus.isGranted || requestStatus.isLimited) {
    return true;
  } else {
    return false;
  }
}

///-------------------- Check Storage Permission -------------------------------
Future<bool> checkStoragePermission(context) async {
  PermissionStatus status = await Permission.storage.status;
  if(status.isGranted || status.isLimited) {
    return true;
  } else {
    return false;
  }
}