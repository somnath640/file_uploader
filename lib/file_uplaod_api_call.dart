import 'dart:convert';
import 'dart:io';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:path/path.dart';
import 'package:http/http.dart' as http;


///------------- Use syntax to get Image as PickedFile -------------------------
/// @ var image = await getImage(context);
/// @ add Additional parameter is required.
/// @ Return type is "File".
/// @ Use Image.file(File file) widget to display file asset for App.
///-----------------------------------------------------------------------------

///---------------------- Packages Dependency ----------------------------------
/// @ http: ^0.13.4
///-----------------------------------------------------------------------------

/// -------------- It will helps to upload files Using API -----------------------
/// @ [Required] file as File
/// @ [Required] dataParameters as Map<String, String>
/// @ [Required] headers as Map<String, String>, use {} for blank header
/// @ [Required] apiSourceUrl as String E.g.- www.google.com/
/// @ [Required] endPath as String E.g.- *upload/file/
/// @ [Required] uploadKey as String
Future<List<dynamic>> uploadFileUsingAPI(BuildContext context,
    File file,
    Map<String, String> dataParameters,
    Map<String, String> headers,
    String apiSourceUrl,
    String endPath,
    String uploadKey) async {
  // String _fileName = basename(file.path);
  String _apiUrl = "$apiSourceUrl$endPath";
  var request = http.MultipartRequest('POST', Uri.parse(_apiUrl));
  request.headers.addAll(headers);
  request.fields.addAll(dataParameters);

  var _uploadFile = await http.MultipartFile.fromPath(uploadKey, file.path);
  request.files.add(_uploadFile);
  var response = await request.send();
  var responseData = await response.stream.toBytes();
  var jsonResponse = String.fromCharCodes(responseData);
  return json.decode(jsonResponse);
}


Future<List<dynamic>> uploadMultipleFilesUsingAPI(BuildContext context,
    List<File> files,
    Map<String, String> dataParameters,
    Map<String, String> headers,
    String apiSourceUrl,
    String endPath,
    String uploadKey) async {

  String _apiUrl = "$apiSourceUrl$endPath";
  var request = http.MultipartRequest('POST', Uri.parse(_apiUrl));
  request.headers.addAll(headers);
  request.fields.addAll(dataParameters);

  await Future.forEach(files, (File file) async {
    var _uploadFile = await http.MultipartFile.fromPath(uploadKey, file.path);
    request.files.add(_uploadFile);
  });


  var response = await request.send();
  var responseData = await response.stream.toBytes();
  var jsonResponse = String.fromCharCodes(responseData);
  return json.decode(jsonResponse);
}
